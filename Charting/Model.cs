﻿using System.Collections.Generic;

namespace Charting
{
    public class Model
    {
        public double A { get; set; } = 1;
        public double B { get; set; } = 0;
        public double C { get; set; } = 0;
        public double XMin { get; set; } = -10;
        public double XMax { get; set; } = 10;
        public double YMin { get; set; }
        public double YMax { get; set; }

        private const double dx = 0.1;

        public IEnumerable<double> AllX()
        {
            for (double x = XMin; x <= XMax; x += dx)
            {
                yield return x;
            }
        }

        public IEnumerable<double> AllY()
        {
            foreach (var x in AllX())
            {
                yield return A * x * x + B * x + C;
            }
        }
    }
}