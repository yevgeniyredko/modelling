﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Charting.Annotations;

namespace Charting
{
    public class ViewModel : INotifyPropertyChanged
    {
        private readonly Model model = new Model();

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<Model> PlotEvent; 

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Plot()
        {
            PlotEvent?.Invoke(model);
        }

        public IEnumerable<double> AllX() => model.AllX();
        public IEnumerable<double> AllY() => model.AllY();

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot, true));

        public double A
        {
            get => model.A;
            set
            {
                model.A = value;
                OnPropertyChanged(nameof(A));
            }
        }

        public double B
        {
            get => model.B;
            set
            {
                model.B = value;
                OnPropertyChanged(nameof(B));
            }
        }

        public double C
        {
            get => model.C;
            set
            {
                model.C = value;
                OnPropertyChanged(nameof(C));
            }
        }

        public double XMin
        {
            get => model.XMin;
            set
            {
                model.XMin = value;
                OnPropertyChanged(nameof(XMin));
            }
        }

        public double XMax
        {
            get => model.XMax;
            set
            {
                model.XMax = value;
                OnPropertyChanged(nameof(XMax));
            }
        }

        public double YMin
        {
            get => model.YMin;
            set
            {
                model.YMin = value;
                OnPropertyChanged(nameof(YMin));
            }
        }

        public double YMax
        {
            get => model.YMax;
            set
            {
                model.YMax = value;
                OnPropertyChanged(nameof(YMax));
            }
        }
    }
}