﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Input;
using Histogram.Annotations;

namespace Histogram
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event Action<(double[] x, double[] y), double, int> PlotEvent;

        public void Plot()
        {
            PlotEvent?.Invoke(GetValues(), 1.0 / m, m);
        }

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot, true));

        public (double[], double[]) GetValues()
        {
            var y = new double[m];
            var rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                var next = rnd.Next(m);
                y[next]++;
            }

            GenerateResult(y);

            for (int i = 0; i < y.Length; i++)
            {
                y[i] /= n;
            }

            var x = GenerateLabels(m);
            return (x, y);
        }

        private static double[] GenerateLabels(int m)
        {
            return Enumerable.Range(0, m).Select(x => x + 0.0).ToArray();
        }

        private void GenerateResult(double[] y)
        {
            var piE = y.Select(x => x / n).Sum() / m;

            PiT = $"{1.0 / m}";
            PiE = $"{piE}";

            OnPropertyChanged(nameof(PiT));
            OnPropertyChanged(nameof(PiE));
        }

        public string PiT { get; private set; }
        public string PiE { get; private set; }

        private int m = 5;

        public int M
        {
            get => m;
            set
            {
                m = value;
                OnPropertyChanged(nameof(M));
            }
        }

        private int n = 100;

        public int N
        {
            get => n;
            set
            {
                n = value;
                OnPropertyChanged(nameof(N));
            }
        }
    }
}