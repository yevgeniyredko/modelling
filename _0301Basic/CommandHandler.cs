﻿using System;
using System.Windows.Input;

namespace _0301Basic
{
    public class CommandHandler : ICommand
    {
        public CommandHandler(Action action)
        {
            this.action = action;
        }

        public void Execute(object parameter) => action();
        
        public bool CanExecute(object parameter) => true;
        
        public event EventHandler CanExecuteChanged;
        private readonly Action action;
    }
}