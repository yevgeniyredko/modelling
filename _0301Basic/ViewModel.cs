﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Media;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Wpf;
using LinearAxis = OxyPlot.Axes.LinearAxis;
using LineSeries = OxyPlot.Series.LineSeries;
using _0301Basic.Annotations;

namespace _0301Basic
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            M = 10;
            N = 1000;
            PlotCommand.Execute(null);
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot));

        private void Plot()
        {
            var model = new PlotModel();
            model.Axes.Clear();
            model.Axes.Add(new LinearAxis
            {
                Title = "N(ρ)",
                IntervalLength = 20,
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });
            model.Axes.Add(new LinearAxis
            {
                Title = "ρ",
                IntervalLength = 20,
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });

            var mathModel = new MathModel(m: M, n: N);

            var mPlus1 = new LineSeries
            {
                Title = "m + 1",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Red.ToOxyColor()
            };
            mPlus1.Points.AddRange(mathModel.MPlus1.Select(p => new DataPoint(p.x, p.y)));
            
            var theory = new LineSeries
            {
                Title = "Теория",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Blue.ToOxyColor()
            };
            theory.Points.AddRange(mathModel.Theory.Select(p => new DataPoint(p.x, p.y)));

            var practice = new LineSeries
            {
                Title = "Практика",
                MarkerType = MarkerType.Circle,
                MarkerFill = Colors.Crimson.ToOxyColor(),
                Color = OxyColor.FromArgb(0, 0, 0, 0)
            };
            practice.Points.AddRange(mathModel.Practice.Select(p => new DataPoint(p.x, p.y)));

            var difference = new LineSeries
            {
                Title = "СКО",
                MarkerType = MarkerType.Circle,
                MarkerFill = Colors.Gold.ToOxyColor(),
                Color = OxyColor.FromArgb(0, 0, 0, 0)
            };
            difference.Points.AddRange(mathModel.Difference.Select(p => new DataPoint(p.x, p.y)));

            model.Series.Add(mPlus1);
            model.Series.Add(theory);
            model.Series.Add(practice);
            model.Series.Add(difference);

            foreach (var ax in model.Axes)
                ax.Minimum = 0;

            PlotModel = model;
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get => plotModel;
            set
            {
                plotModel = value;
                OnPropertyChanged(nameof(PlotModel));
            }
        }

        private int n;
        public int N
        {
            get => n;
            set
            {
                n = value;
                OnPropertyChanged(nameof(N));
            }
        }
        
        private int m;
        public int M
        {
            get => m;
            set
            {
                m = value;
                OnPropertyChanged(nameof(M));
            }
        }
    }
}