﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _0301Basic
{
    public class MathModel
    {   
        public MathModel(int m, int n)
        {
            this.m = m;
            this.n = n;

            CalculateThisShit();
        }

        public IEnumerable<(double x, double y)> MPlus1 => xList.Select(x => (x, (double) m + 1));
        
        public IEnumerable<(double x, double y)> Theory
        {
            get
            {
                for (var i = 0; i < Math.Min(xList.Count, yTheoryList.Count); i++)
                {
                    yield return (xList[i], yTheoryList[i]);
                }
            }
        }
        
        public IEnumerable<(double x, double y)> Practice
        {
            get
            {
                for (var i = 0; i < Math.Min(xList.Count, yPracticeList.Count); i++)
                {
                    yield return (xList[i], yPracticeList[i]);
                }
            }
        }
        
        public IEnumerable<(double x, double y)> Difference
        {
            get
            {
                for (var i = 0; i < Math.Min(xList.Count, differenceList.Count); i++)
                {
                    yield return (xList[i], differenceList[i]);
                }
            }
        }
        
        private readonly int m;
        private readonly int n;
        
        private IReadOnlyList<double> xList;
        private IReadOnlyList<double> yTheoryList;
        private IReadOnlyList<double> yPracticeList;
        private readonly List<double> differenceList = new List<double>();

        private const double RoMax = 10;
        private const double Dx = 0.1;

        private void CalculateThisShit()
        {
            var xs = new List<double>();
            for (double x = 0; x <= RoMax; x += Dx)
                xs.Add(x);
            xList = xs;
            
            var yTheory = new List<double>();
            for (double x = 0; x <= RoMax; x += Dx)
                yTheory.Add(DecideOfTheory(x, Ro0(m, x), m));
            yTheoryList = yTheory;

            var yPractice = new List<double>();
            for (double x = 0; x <= RoMax; x += Dx)
            {
                differenceList.Add(0);
                yPractice.Add(DecideOfExperiment(x, m, n));
            }
            yPracticeList = yPractice;
        }
        
        private static double DecideOfTheory(double ro, double ro0, double m)
        {
            double result = 0;

            for (var k = 0; k <= m + 1; k++)
                result += k * ro0 * Math.Pow(ro, k);

            return result;
        }

        private static double Ro0(double m, double ro)
        {
            var result = (1 - ro) / (1 - Math.Pow(ro, m + 2));
            return result;
        }

        private double DecideOfExperiment(double ro, double m, double n) 
        {
            var mu = 1;
            var lambda = ro * mu;
            var applications = 0.0;
            var queue = 0;

            var rnd = new Random();

            var tauComing = 0.0;
            var tauService = 0.0;

            var onMaintenance = false;

            for (var i = 0; i <= n; i++)
            {
                var measuresCount = 0;
                while (true)
                {
                    const double eps = 1e-9;
                    if (Math.Abs(tauComing) < eps)
                    {
                        if (!onMaintenance && queue == 0)
                        {
                            tauComing = -1 / lambda * Math.Log(rnd.NextDouble());
                            tauService = -1.0 / mu * Math.Log(rnd.NextDouble());
                            onMaintenance = true;
                        }
                        else if (queue < m + 1)
                        {
                            queue++;
                            tauComing = -1 / lambda * Math.Log(rnd.NextDouble());
                        }
                        else
                        {
                            tauComing = -1 / lambda * Math.Log(rnd.NextDouble());
                        }
                    }

                    if (Math.Abs(tauService) < eps)
                    {
                        if (queue > 0)
                        {
                            queue--;
                            tauService = -1.0 / mu * Math.Log(rnd.NextDouble());
                            onMaintenance = true;
                        }
                        else
                        {
                            onMaintenance = false;
                        }
                    }

                    if (onMaintenance)
                    {
                        if (tauComing > tauService)
                        {
                            tauComing -= tauService;
                            tauService = 0;
                        }
                        else if (tauComing < tauService)
                        {
                            tauService -= tauComing;
                            tauComing = 0;
                        }
                    }
                    else
                    {
                        tauComing = 0;
                    }

                    if (onMaintenance && Math.Abs(tauService) < eps)
                    {
                        measuresCount++;

                        if (measuresCount == 10)
                        {
                            differenceList[differenceList.Count - 1] +=
                                (yTheoryList[differenceList.Count - 1] - queue) *
                                (yTheoryList[differenceList.Count - 1] - queue);
                            applications += queue;
                            break;
                        }
                        onMaintenance = false;
                    }
                }
            }

            differenceList[differenceList.Count - 1] *= 1.0 / (n - 1);
            differenceList[differenceList.Count - 1] = Math.Sqrt(differenceList[differenceList.Count - 1]);
            
            return applications / n;
        }

    }
}