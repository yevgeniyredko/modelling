﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Media;
using Discrete;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Wpf;
using _0203GuaranteedResult.Annotations;
using LinearAxis = OxyPlot.Axes.LinearAxis;
using LineSeries = OxyPlot.Series.LineSeries;

namespace Charting
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            A = 0;
            B = 27;
            Alpha = 1.5;
            Beta = 1.5;
            QGuaranteed = 0;
            XGuaranteed = 0;
            PlotCommand.Execute(null);
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Plot()
        {
            var model = new PlotModel();
            model.Axes.Clear();
            model.Axes.Add(new LinearAxis
            {
                Title = "Y",
                IntervalLength = 20,
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });
            model.Axes.Add(new LinearAxis
            {
                Title = "X",
                IntervalLength = 20,
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });

            var mathModel = new MathModel((int)A, (int)B, Alpha, Beta);

            var qSeries = new LineSeries
            {
                Title = "Гарантированный рез-т",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Red.ToOxyColor()
            };
            qSeries.Points.AddRange(mathModel.Q.Select(p => new DataPoint(p.x, p.y)));
            
            var qTheory = new LineSeries
            {
                Title = "Теория",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Blue.ToOxyColor()
            };
            qTheory.Points.AddRange(mathModel.QTheory.Select(p => new DataPoint(p.x, p.y)));

            model.Series.Add(qSeries);
            model.Series.Add(qTheory);

            foreach (var ax in model.Axes)
            {
                if (ax.Title == "X") 
                    ax.Minimum = A;
                else 
                    ax.Minimum = 0;
            }
            
            PlotModel = model;

            XGuaranteed = mathModel.XGuaranteed;
            QGuaranteed = mathModel.QGuaranteed;
        }

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot, true));

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get => plotModel;
            set
            {
                plotModel = value;
                OnPropertyChanged(nameof(PlotModel));
            }
        }

        private double a;
        public double A
        {
            get => a;
            set
            {
                a = value;
                OnPropertyChanged(nameof(A));
            }
        }

        private double b;
        public double B
        {
            get => b;
            set
            {
                b = value;
                OnPropertyChanged(nameof(B));
            }
        }

        private double alpha;
        public double Alpha
        {
            get => alpha;
            set
            {
                alpha = value;
                OnPropertyChanged(nameof(Alpha));
            }
        }

        private double beta;
        public double Beta
        {
            get => beta;
            set
            {
                beta = value;
                OnPropertyChanged(nameof(Beta));
            }
        }

        private double xGuaranteed;
        public double XGuaranteed
        {
            get => xGuaranteed;
            set
            {
                xGuaranteed = value;
                OnPropertyChanged(nameof(XGuaranteed));
            }
        }

        private double qGuaranteed;
        public double QGuaranteed
        {
            get => qGuaranteed;
            set
            {
                qGuaranteed = value;
                OnPropertyChanged(nameof(QGuaranteed));
            }
        }
    }
}