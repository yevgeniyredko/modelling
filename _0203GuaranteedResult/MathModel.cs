﻿using System.Collections.Generic;
using System.Linq;

namespace Discrete
{
    public class MathModel
    {
        private readonly int a;
        private readonly int b;
        private readonly double alpha;
        private readonly double beta;

        public MathModel(int a, int b, double alpha, double beta)
        {
            this.a = a;
            this.b = b;
            this.alpha = alpha;
            this.beta = beta;
            XGuaranteed = (alpha * b + beta * a) / (beta + alpha);
            QGuaranteed = alpha * (b - XGuaranteed);
        }

        public IEnumerable<(double x, double y)> QTheory
        {
            get
            {
                double GetQ(double x, double y) => y > x ? alpha * (y - x) : beta * (x - y);
                var pIth = 1.0 / (b - a + 1);
                for (var x = a; x <= b; x++)
                    yield return (x, Enumerable.Range(a, b - a + 1).Select(y => GetQ(x, y) * pIth).Sum());
            }
        }
        
        public IEnumerable<(double x, double y)> Q
        {
            get
            {
                for (double x = a; x <= b; x += 0.1)
                    yield return x < XGuaranteed
                        ? (x, alpha * (b - x))
                        : (x, beta * (x - a));
            }
        }

        public double XGuaranteed { get; }
        public double QGuaranteed { get; }
    }
}