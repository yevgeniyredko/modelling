﻿using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Wpf;
using PropertyChanged;
using LinearAxis = OxyPlot.Axes.LinearAxis;
using LineSeries = OxyPlot.Series.LineSeries;

namespace _0302Erlang
{
    [AddINotifyPropertyChangedInterface]
    public class ViewModel 
    {
        public ViewModel()
        {
            M = 8;
            N = 100;
            R1 = 21;
            R2 = 24;
            PlotCommand.Execute(null);
        }

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot));

        private void Plot()
        {
            var model = new PlotModel();
            model.Axes.Clear();
            model.Axes.Add(new LinearAxis
            {
                Title = "N(ρ)",
                IntervalLength = 20,
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });
            model.Axes.Add(new LinearAxis
            {
                Title = "ρ",
                IntervalLength = 20,
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });

            var mathModel = new MathModel(m: M, n: N, r1: R1, r2: R2, roMax: 5);

            var mPlus1 = new LineSeries
            {
                Title = "m + 1",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Chartreuse.ToOxyColor()
            };
            mPlus1.Points.AddRange(mathModel.MPlus1.Select(p => new DataPoint(p.x, p.y)));
            
            var theory = new LineSeries
            {
                Title = "Теория",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Blue.ToOxyColor()
            };
            theory.Points.AddRange(mathModel.Theory.Select(p => new DataPoint(p.x, p.y)));

            var practice = new LineSeries
            {
                Title = "Практика",
                MarkerType = MarkerType.Circle,
                MarkerFill = Colors.Crimson.ToOxyColor(),
                MarkerStrokeThickness = 0.5,
                Color = OxyColor.FromArgb(0, 0, 0, 0)
            };
            practice.Points.AddRange(mathModel.Practice.Select(p => new DataPoint(p.x, p.y)));

            var difference = new LineSeries
            {
                Title = "СКО",
                MarkerType = MarkerType.Circle,
                MarkerFill = Colors.Gold.ToOxyColor(),
                Color = OxyColor.FromArgb(0, 0, 0, 0)
            };
            difference.Points.AddRange(mathModel.Difference.Select(p => new DataPoint(p.x, p.y)));

            model.Series.Add(mPlus1);
            model.Series.Add(theory);
            model.Series.Add(practice);
            model.Series.Add(difference);

            foreach (var ax in model.Axes)
                ax.Minimum = 0;

            PlotModel = model;
        }

        public PlotModel PlotModel { get; set; }
        public int N { get; set; }
        public int M { get; set; }
        public int R1 { get; set; }
        public int R2 { get; set; }
    }
}