﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _0302Erlang
{
    public class MathModel
    {   
        public MathModel(int m, int n, int r1, int r2, double roMax)
        {
            this.m = m;
            this.n = n;
            this.r1 = r1;
            this.r2 = r2;
            this.roMax = roMax;

            CalculateThisShit();
        }

        public IEnumerable<(double x, double y)> MPlus1 => xList.Select(x => (x, (double) m + 1));
        
        public IEnumerable<(double x, double y)> Theory
        {
            get
            {
                for (var i = 0; i < Math.Min(xList.Count, yTheoryList.Count); i++)
                {
                    yield return (xList[i], yTheoryList[i]);
                }
            }
        }
        
        public IEnumerable<(double x, double y)> Practice
        {
            get
            {
                for (var i = 0; i < Math.Min(xList.Count, yPracticeList.Count); i++)
                {
                    yield return (xList[i], yPracticeList[i]);
                }
            }
        }
        
        public IEnumerable<(double x, double y)> Difference
        {
            get
            {
                for (var i = 0; i < Math.Min(xList.Count, differenceList.Count); i++)
                {
                    yield return (xList[i], differenceList[i]);
                }
            }
        }
        
        private readonly int m;
        private readonly int n;
        private readonly int r1;
        private readonly int r2;
        private readonly double roMax;

        private IReadOnlyList<double> xList;
        private IReadOnlyList<double> yTheoryList;
        private IReadOnlyList<double> yPracticeList;
        private readonly List<double> differenceList = new List<double>();

        private const double dx = 0.1;

        private void CalculateThisShit()
        {
            var xs = new List<double>();
            for (double ro = 0; ro <= roMax; ro += dx)
                xs.Add(ro);
            xList = xs;
            
            var yTheory = new List<double>();
            for (double ro = 0; ro <= roMax; ro += dx)
                yTheory.Add(GetTheoryValue(ro));
            yTheoryList = yTheory;

            var yPractice = new List<double>();
            for (double ro = 0; ro <= roMax; ro += dx)
            {
                differenceList.Add(0);
                yPractice.Add(GetPracticeValue(ro));
            }
            yPracticeList = yPractice;
        }
        
        private double GetTheoryValue(double ro)
        {
            double Ro0(double ro_) => (1 - ro_) / (1 - Math.Pow(ro_, m + 2));

            var ro0 = Ro0(ro);
            var result = 0.0;
            for (var r = 0; r <= m + 1; r++)
                result += r * ro0 * Math.Pow(ro, r);
            return result;
        }

        private double GetPracticeValue(double ro) 
        {
            var mu = 1;
            var lambda = ro * mu;

            double GetTauComing()
            {
                var result = 0.0;
                for (var i = 0; i < r1; i++)
                    result += -1.0 / lambda * Math.Log(Helpers.Random());
                return result / r1;
            }

            double GetTauService()
            {
                var result = 0.0;
                for (var i = 0; i < r2; i++)
                    result += -1.0 / mu * Math.Log(Helpers.Random());
                return result / r2;
            }

            var applications = 0.0;
            var queue = 0;

            var tauComing = 0.0;
            var tauService = 0.0;

            var onMaintenance = false;

            for (var i = 0; i <= n; i++)
            {
                var measuresCount = 0;
                while (true)
                {
                    if (tauComing.IsZero())
                    {
                        if (!onMaintenance && queue == 0)
                        {
                            tauService = GetTauService();
                            onMaintenance = true;
                        }
                        else 
                        {
                            if (queue < m + 1) queue++;
                        }
                        tauComing = GetTauComing();
                    }

                    if (tauService.IsZero())
                    {
                        if (queue > 0)
                        {
                            queue--;
                            tauService = GetTauService();
                            onMaintenance = true;
                        }
                        else
                        {
                            onMaintenance = false;
                        }
                    }

                    if (onMaintenance)
                    {
                        if (tauComing > tauService)
                        {
                            tauComing -= tauService;
                            tauService = 0;
                        }
                        else if (tauComing < tauService)
                        {
                            tauService -= tauComing;
                            tauComing = 0;
                        }
                    }
                    else
                    {
                        tauComing = 0;
                    }

                    if (onMaintenance && tauService.IsZero())
                    {
                        measuresCount++;

                        const int maxMeasuresCount = 10;
                        if (measuresCount == maxMeasuresCount)
                        {
                            differenceList[differenceList.Count - 1] +=
                                (yTheoryList[differenceList.Count - 1] - queue) *
                                (yTheoryList[differenceList.Count - 1] - queue);
                            applications += queue;
                            break;
                        }
                        onMaintenance = false;
                    }
                }
            }

            differenceList[differenceList.Count - 1] *= 1.0 / (n - 1);
            differenceList[differenceList.Count - 1] = Math.Sqrt(differenceList[differenceList.Count - 1]);
            
            return applications / n;
        }

    }
}