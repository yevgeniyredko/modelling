﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0302Erlang
{
    static class Helpers
    {
        public static bool IsZero(this double d) => Math.Abs(d) < 1e-9;

        private static readonly Random rnd = new Random();
        public static double Random() => rnd.NextDouble();
    }
}
