﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace Discrete
{
    public class MathModel
    {
        public MathModel(int a, int b, double alpha, double beta, int n, int p)
        {
            this.a = a;
            this.b = b;
            this.alpha = alpha;
            this.beta = beta;
            this.n = n;
            this.p = p;
            qTheory = new double[b - a + 1];
            qPractice = new double[b - a + 1];
            difference = new double[b - a + 1];
            CalculateThisShit();
        }

        public IReadOnlyList<double> QTheory => qTheory;
        public IReadOnlyList<double> QPractice => qPractice;
        public IReadOnlyList<double> Difference => difference;

        public double XStarTheory { get; private set; }
        public double XStarPractice { get; private set; }
        public double QStarTheory { get; private set; }
        public double QStarPractice { get; private set; }
        public double S { get; private set; }

        private readonly int p;

        private readonly int a;
        private readonly int b;
        private readonly double alpha;
        private readonly double beta;
        private readonly int n;
                
        private readonly double[] qTheory;
        private readonly double[] qPractice;
        private readonly double[] difference;
        
        private void CalculateThisShit()
        {
            double GetQ(double x, double y) => y > x ? alpha * (y - x) : beta * (x - y);

            double GetQPractice(int x, double y, bool writeDifference)
            {
                var q = GetQ(x, y);
                if (writeDifference) 
                    difference[x - a] += (q - qTheory[x - a]) * (q - qTheory[x - a]);
                return q;
            }

            for (var x = a; x <= b; x++)
                qTheory[x - a] = 1.0 / (b - a) * (alpha * (x - b) * (x - b) / 2.0 + beta * (x - a) * (x - a) / 2.0);

            (QStarTheory, XStarTheory) = (qTheory.Min(), (alpha * b + beta * a) / (alpha + beta));

            var rnd = new Random();
            for (var x = a; x <= b; x++)
            {
                qPractice[x - a] = Enumerable
                                       .Repeat(0, n)
                                       .Select(i => a + rnd.NextDouble() * (b - a) + i)
                                       .Select(y => rnd.Next(100) < p ? GetQPractice(0, y, false) : GetQPractice(x, y, true))
                                       .Sum() / n;
            }

            (QStarPractice, XStarPractice) = qPractice.Select((q, x) => (q, x)).Min();

            for (var i = 0; i < difference.Length; i++)
            {
                difference[i] *= 1.0 / (n - 1);
                difference[i] = Math.Sqrt(difference[i]);
            }
            
            S = difference.Sum() / difference.Length;
        }
    }
}