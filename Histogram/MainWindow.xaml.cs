﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InteractiveDataDisplay.WPF;

namespace Histogram
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var viewModel = (ViewModel) DataContext;
            viewModel.PlotEvent += (res, p, a, b, m) =>
            {
                BarChart.Description = "Pi";
                BarChart.PlotBars(res.x, res.y);
                BarChart.BarsWidth = (double) (b - a) / m;
                LineGraph.Description = "Pi(теор)";
                LineGraph.Plot(Enumerable.Range(a, b - a + 1), Enumerable.Repeat(p, b - a + 1));
            };
        }

        private void UiPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            var regex = new Regex("[^0-9.-]+");
            return !regex.IsMatch(text);
        }
    }
}
