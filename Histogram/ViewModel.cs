﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Input;
using Histogram.Annotations;

namespace Histogram
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event Action<(double[] x, double[] y), double, int, int, int> PlotEvent;

        public void Plot()
        {
            PlotEvent?.Invoke(GetValues(), 1.0 / (b - a) * (b - a) / m, a, b, m);
        }

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot, true));

        public (double[], double[]) GetValues()
        {
            var y = new double[m];
            var rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                var next = rnd.Next(m);
                y[next]++;
            }

            for (int i = 0; i < y.Length; i++)
            {
                y[i] /= n;
            }

            var x = GenerateLabels(m, a, b);
            GenerateResult(y);
            return (x, y);
        }

        private static double[] GenerateLabels(int m, double a, double b)
        {
            var result = new double[m];

            var delta = (b - a) / m;
            var current = a;
            for (int i = 0; i < m; i++)
            {
                result[i] = (current + current + delta) / 2;
                current += delta;
            }

            return result;
        }

        private void GenerateResult(double[] y)
        {
            var mx = y.Sum() / y.Length;
            var dx = y.Select(x => (x - mx) * (x - mx)).Sum() / (n - 1);
            var sx = Math.Sqrt(dx);

            Res = $"{mx:F2} ± {sx:F8}";
            OnPropertyChanged(nameof(Res));

            Dx = $"{dx:F8}";
            OnPropertyChanged(nameof(Dx));
        }

        public string Res { get; private set; }
        public string Dx { get; private set; }

        private int a = 0;

        public int A
        {
            get => a;
            set
            {
                a = value;
                OnPropertyChanged(nameof(A));
            }
        }

        private int b = 10;

        public int B
        {
            get => b;
            set
            {
                b = value;
                OnPropertyChanged(nameof(B));
            }
        }

        private int m = 5;

        public int M
        {
            get => m;
            set
            {
                m = value;
                OnPropertyChanged(nameof(M));
            }
        }

        private int n = 100;

        public int N
        {
            get => n;
            set
            {
                n = value;
                OnPropertyChanged(nameof(N));
            }
        }
    }
}