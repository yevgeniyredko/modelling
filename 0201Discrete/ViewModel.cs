﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Media;
using Discrete;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Wpf;
using _0201Discrete.Annotations;
using LinearAxis = OxyPlot.Axes.LinearAxis;
using LineSeries = OxyPlot.Series.LineSeries;

namespace Charting
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            A = 0;
            B = 27;
            Alpha = 1.5;
            Beta = 1.5;
            N = 10000;
            P = 40;
            QStarTheory = 0;
            XStarTheory = 0;
            QStarPractice = 0;
            XStarPractice = 0;
            S = 0;
            PlotCommand.Execute(null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Plot()
        {
            var model = new PlotModel() {};
            model.Axes.Clear();
            model.Axes.Add(new LinearAxis
            {
                Title = "Y",
                IntervalLength = 20,
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });
            model.Axes.Add(new LinearAxis
            {
                Title = "X",
                IntervalLength = 20,
                Position = AxisPosition.Bottom,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.LightGray
            });

            var mathModel = new MathModel((int)A, (int)B, Alpha, Beta, (int)N, P);

            var theory = new LineSeries
            {
                Title = "Теория",
                LineStyle = LineStyle.Solid,
                Smooth = true,
                Color = Colors.Blue.ToOxyColor()
            };
            theory.Points.AddRange(mathModel.QTheory.Select((y, i) => new DataPoint(i + A, y)));

            var practice = new LineSeries
            {
                Title = "Практика",
                MarkerType = MarkerType.Circle,
                MarkerFill = Colors.Red.ToOxyColor(),
                Color = OxyColor.FromArgb(0, 0, 0, 0)
            };
            practice.Points.AddRange(mathModel.QPractice.Select((y, i) => new DataPoint(i + A, y)));

            var difference = new LineSeries
            {
                Title = "СКО",
                MarkerType = MarkerType.Circle,
                MarkerFill = Colors.Gold.ToOxyColor(),
                Color = OxyColor.FromArgb(0, 0, 0, 0)
            };
            difference.Points.AddRange(mathModel.Difference.Select((y, i) => new DataPoint(i + A, y)));

            model.Series.Add(theory);
            model.Series.Add(practice);
            model.Series.Add(difference);

            foreach (var ax in model.Axes)
            {
                if (ax.Title == "X") 
                    ax.Minimum = A;
                else 
                    ax.Minimum = 0;
            }
            
            PlotModel = model;

            XStarTheory = mathModel.XStarTheory;
            XStarPractice = mathModel.XStarPractice;
            QStarTheory = mathModel.QStarTheory;
            QStarPractice = mathModel.QStarPractice;
            S = mathModel.S;
        }

        private ICommand plotCommand;
        public ICommand PlotCommand => plotCommand ?? (plotCommand = new CommandHandler(Plot, true));

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get => plotModel;
            set
            {
                plotModel = value;
                OnPropertyChanged(nameof(PlotModel));
            }
        }

        private double a;
        public double A
        {
            get => a;
            set
            {
                a = value;
                OnPropertyChanged(nameof(A));
            }
        }

        private double b;
        public double B
        {
            get => b;
            set
            {
                b = value;
                OnPropertyChanged(nameof(B));
            }
        }

        private double alpha;
        public double Alpha
        {
            get => alpha;
            set
            {
                alpha = value;
                OnPropertyChanged(nameof(Alpha));
            }
        }

        private double beta;
        public double Beta
        {
            get => beta;
            set
            {
                beta = value;
                OnPropertyChanged(nameof(Beta));
            }
        }

        private double n;
        public double N
        {
            get => n;
            set
            {
                n = value;
                OnPropertyChanged(nameof(N));
            }
        }

        private double xStarTheory;
        public double XStarTheory
        {
            get => xStarTheory;
            set
            {
                xStarTheory = value;
                OnPropertyChanged(nameof(XStarTheory));
            }
        }

        private double xStarPractice;
        public double XStarPractice
        {
            get => xStarPractice;
            set
            {
                xStarPractice = value;
                OnPropertyChanged(nameof(XStarPractice));
            }
        }

        private double qStarTheory;
        public double QStarTheory
        {
            get => qStarTheory;
            set
            {
                qStarTheory = value;
                OnPropertyChanged(nameof(QStarTheory));
            }
        }

        private double qStarPractice;
        public double QStarPractice
        {
            get => qStarPractice;
            set
            {
                qStarPractice = value;
                OnPropertyChanged(nameof(QStarPractice));
            }
        }
        
        private double s;
        public double S
        {
            get => s;
            set
            {
                s = value;
                OnPropertyChanged(nameof(S));
            }
        }

        private readonly int P;
    }
}